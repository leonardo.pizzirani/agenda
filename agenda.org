# -*- org-confirm-babel-evaluate: nil -*-
#+html_head: <link rel="stylesheet" type="text/css" href="agenda.css" />
#+options: num:nil
#+options: toc:nil
#+options: html-postamble:nil
#+title: レオナルド \\
#+title: Homepage \\
#+title: ピチラーニ
#+author: Leonardo Pizzirani

#+begin_center
#+begin_src elisp :exports results
(defun random-emoticon()
  (+ 128512 (random (* 16 5))))
(message "%c %s %s"
  (random-emoticon) "Today is" (format-time-string "%A %d %B %Y"))
#+end_src
#+end_center
